package com.personal.projects.climbing.userservice.util;

import com.personal.projects.climbing.userservice.data.ChangePasswordRequest;
import com.personal.projects.climbing.userservice.data.RegisterUserRequest;

/**
 * Utility methods to be used throughout the tests.
 *
 * @author Chris Smith
 */
public final class TestUtils {
    /**
     * Default private constructor to prevent instantiation
     */
    private TestUtils() {

    }

    /**
     * Creates a dummy {@link RegisterUserRequest} to be used in the tests
     *
     * @return the {@link RegisterUserRequest}
     */
    public static RegisterUserRequest createDummyRegisterUserRequest() {
        RegisterUserRequest request = new RegisterUserRequest();
        request.setUsername("Joe Bloggs");
        request.setPassword("raw password");
        return request;
    }

    /**
     * Creates a dummy {@link ChangePasswordRequest} to be used in the tests
     *
     * @return the {@link ChangePasswordRequest}
     */
    public static ChangePasswordRequest createDummyChangePasswordRequest() {
        ChangePasswordRequest request = new ChangePasswordRequest();
        request.setUsername("Joe Bloggs");
        request.setCurrentPassword("current-password");
        request.setNewPassword("new-password");
        return request;
    }
}
