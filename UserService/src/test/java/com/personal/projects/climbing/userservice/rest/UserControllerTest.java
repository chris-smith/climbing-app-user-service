package com.personal.projects.climbing.userservice.rest;

import com.personal.projects.climbing.userservice.data.ChangePasswordRequest;
import com.personal.projects.climbing.userservice.data.RegisterUserRequest;
import com.personal.projects.climbing.userservice.processor.UserProcessor;
import com.personal.projects.climbing.userservice.util.TestUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

/**
 * Unit tests for the {@link UserController} class.
 *
 * @author Chris Smith
 */
public class UserControllerTest {
    private int userId = 100;
    private String username = "jbloggs";
    private String password = "password";

    @Mock
    private UserProcessor processor;

    @InjectMocks
    private UserController controller;

    /**
     * Initialises the mocks
     */
    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Tests that {@link UserController#registerNewUser(RegisterUserRequest)} returns the expected response when
     * supplied with a valid {@link RegisterUserRequest}
     */
    @Test
    public void registerNewUser_validRequest_returnsExpectedResponse() {
        // Arrange
        RegisterUserRequest request = TestUtils.createDummyRegisterUserRequest();
        when(this.processor.processNewUserRequest(request)).thenReturn(this.userId);

        // Act
        ResponseEntity<?> response = this.controller.registerNewUser(request);
        int userId = (int) response.getBody();

        // Assert
        verify(this.processor, times(1)).processNewUserRequest(request);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(this.userId, userId);
    }

    /**
     * Tests that {@link UserController#registerNewUser(RegisterUserRequest)} returns a
     * {@link HttpStatus#INTERNAL_SERVER_ERROR} response when there is a problem connecting to the database
     */
    @Test
    public void registerNewUser_errorConnectingToDatabase_returns500ServerError() {
        // Arrange
        RegisterUserRequest request = TestUtils.createDummyRegisterUserRequest();
        String expectedErrorMessage = String.format(
                "Error saving user with username %s to the database",
                request.getUsername());
        when(this.processor.processNewUserRequest(request))
                .thenThrow(new DataAccessException("Error connecting to database") {
                });

        // Act
        ResponseEntity<?> response = this.controller.registerNewUser(request);

        // Assert
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
        assertEquals(expectedErrorMessage, response.getBody());
    }

    /**
     * Tests that {@link UserController#logIn(String, String)} returns the expected response when the supplied user
     * credentials are valid
     */
    @Test
    public void logIn_validCredentials_returnsExpectedResponse() {
        // Arrange
        when(this.processor.processLogInRequest(this.username, this.password)).thenReturn(this.userId);

        // Act
        ResponseEntity<?> response = this.controller.logIn(this.username, this.password);
        Integer userId = (int) response.getBody();

        // Assert
        verify(this.processor, times(1)).processLogInRequest(this.username, this.password);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(this.userId, userId);
    }

    /**
     * Tests that {@link UserController#logIn(String, String)} returns a {@link HttpStatus#NOT_FOUND} response when
     * the credentials supplied are not correct for the associated user
     */
    @Test
    public void logIn_invalidCredentials_returns404NotFound() {
        // Arrange
        String expectedErrorMessage = String.format("Authentication failed for user with username %s", this.username);
        when(this.processor.processLogInRequest(this.username, this.password)).thenReturn(null);

        // Act
        ResponseEntity<?> response = this.controller.logIn(this.username, this.password);

        // Assert
        verify(this.processor, times(1)).processLogInRequest(this.username, this.password);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertEquals(expectedErrorMessage, response.getBody());
    }

    /**
     * Tests that {@link UserController#logIn(String, String)} returns a {@link HttpStatus#INTERNAL_SERVER_ERROR}
     * response when there is a problem connecting to the database
     */
    @Test
    public void logIn_errorConnectingToDatabase_returns500ServerError() {
        // Arrange
        String expectedErrorMessage = String.format(
                "Error retrieving user with username %s from database",
                this.username);
        when(this.processor.processLogInRequest(this.username, this.password))
                .thenThrow(new DataAccessException("Error connecting to database") {
                });

        // Act
        ResponseEntity<?> response = this.controller.logIn(this.username, this.password);

        // Assert
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
        assertEquals(expectedErrorMessage, response.getBody());
    }

    /**
     * Tests that {@link UserController#changePassword(ChangePasswordRequest)} returns the expected response when the
     * request contains valid user credentials
     */
    @Test
    public void changePassword_validRequest_returnsExpectedResponse() {
        // Arrange
        ChangePasswordRequest request = TestUtils.createDummyChangePasswordRequest();
        when(this.processor.processChangePasswordRequest(
                request.getUsername(),
                request.getCurrentPassword(),
                request.getNewPassword()))
                .thenReturn(true);

        // Act
        ResponseEntity<?> response = this.controller.changePassword(request);

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        verify(this.processor, times(1)).processChangePasswordRequest(
                request.getUsername(),
                request.getCurrentPassword(),
                request.getNewPassword());
    }

    /**
     * Tests that {@link UserController#changePassword(ChangePasswordRequest)} returns a {@link HttpStatus#NOT_FOUND}
     * response when the username and current password supplied do not match what's in the database for that user
     */
    @Test
    public void changePassword_incorrectCurrentPasswordSupplied_returns404NotFound() {
        // Arrange
        ChangePasswordRequest request = TestUtils.createDummyChangePasswordRequest();
        String expectedErrorMessage = String.format(
                "Invalid credentials supplied for user with username %s",
                request.getUsername());

        when(this.processor.processChangePasswordRequest(
                request.getUsername(),
                request.getCurrentPassword(),
                request.getNewPassword()))
                .thenReturn(false);

        // Act
        ResponseEntity<?> response = this.controller.changePassword(request);

        // Assert
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertEquals(expectedErrorMessage, response.getBody());
        verify(this.processor, times(1)).processChangePasswordRequest(
                request.getUsername(),
                request.getCurrentPassword(),
                request.getNewPassword());
    }

    /**
     * Tests that {@link UserController#changePassword(ChangePasswordRequest)} returns a
     * {@link HttpStatus#INTERNAL_SERVER_ERROR} response when there is a problem connecting to the database
     */
    @Test
    public void changePassword_errorConnectingToDatabase_returns500ServerError() {
        // Arrange
        ChangePasswordRequest request = TestUtils.createDummyChangePasswordRequest();
        String expectedErrorMessage = String.format(
                "Error updating user %s's password in database",
                request.getUsername());
        when(this.processor.processChangePasswordRequest(
                request.getUsername(),
                request.getCurrentPassword(),
                request.getNewPassword()))
                .thenThrow(new DataAccessException("Error connecting to database") {
                });

        // Act
        ResponseEntity<?> response = this.controller.changePassword(request);

        // Assert
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
        assertEquals(expectedErrorMessage, response.getBody());
    }
}
