package com.personal.projects.climbing.userservice.processor;

import com.personal.projects.climbing.userservice.data.RegisterUserRequest;
import com.personal.projects.climbing.userservice.data.User;
import com.personal.projects.climbing.userservice.datastore.UserDataStore;
import com.personal.projects.climbing.userservice.util.TestUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/**
 * Unit tests for the {@link UserProcessor} class.
 *
 * @author Chris Smith
 */
public class UserProcessorTest {
    @Mock
    private UserDataStore dataStore;

    @Mock
    private BCryptPasswordEncoder passwordEncoder;

    @InjectMocks
    private UserProcessor processor;

    /**
     * Initialises the mocks
     */
    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Tests that {@link UserProcessor#processNewUserRequest(RegisterUserRequest)} calls
     * {@link UserDataStore#insertUser(String, String)} the expected number of times when supplied with a valid
     * {@link RegisterUserRequest}
     */
    @Test
    public void processNewUserRequest_validRequest_callsDataStoreExpectedNoOfTimes() {
        // Arrange
        RegisterUserRequest request = TestUtils.createDummyRegisterUserRequest();
        String encodedPassword = "encoded-password";
        when(this.passwordEncoder.encode(anyString())).thenReturn(encodedPassword);

        // Act
        this.processor.processNewUserRequest(request);

        // Assert
        verify(this.dataStore, times(1)).insertUser(request.getUsername(), encodedPassword);
    }

    /**
     * Tests that {@link UserProcessor#processLogInRequest(String, String)} returns a null when there is no user in
     * the database with the supplied username
     */
    @Test
    public void processLogInRequest_noMatchingUsername_returnsNull() {
        // Arrange
        String username = "jbloggs";
        String rawPassword = "password";
        when(this.dataStore.getUser(username)).thenReturn(null);

        // Act
        Integer userId = this.processor.processLogInRequest(username, rawPassword);

        // Assert
        verify(this.dataStore, times(1)).getUser(username);
        assertNull(userId);
    }

    /**
     * Tests that {@link UserProcessor#processLogInRequest(String, String)} returns the expected user ID when valid
     * user credentials are supplied for a specific user
     */
    @Test
    public void processLogInRequest_validCredentials_returnsExpectedValue() {
        // Arrange
        int expectedUserId = 5;
        String username = "jbloggs";
        String rawPassword = "password";

        User user = new User();
        user.setId(expectedUserId);
        user.setUsername(username);
        user.setEncodedPassword("a7jndsg9");

        when(this.dataStore.getUser(username)).thenReturn(user);
        when(this.passwordEncoder.matches(anyString(), anyString())).thenReturn(true);

        // Act
        Integer actualUserId = this.processor.processLogInRequest(username, rawPassword);

        // Assert
        verify(this.dataStore, times(1)).getUser(username);
        assertEquals(expectedUserId, actualUserId);
    }

    /**
     * Tests that {@link UserProcessor#processLogInRequest(String, String)} returns a null when there is a user in
     * the database with the supplied username but the password provided is incorrect
     */
    @Test
    public void processLogInRequest_invalidCredentials_returnsNull() {
        // Arrange
        String username = "jbloggs";
        String rawPassword = "password";

        User user = new User();
        user.setUsername(username);
        user.setEncodedPassword("a7jndsg9");

        when(this.dataStore.getUser(username)).thenReturn(user);
        when(this.passwordEncoder.matches(anyString(), anyString())).thenReturn(false);

        // Act
        Integer userId = this.processor.processLogInRequest(username, rawPassword);

        // Assert
        verify(this.dataStore, times(1)).getUser(username);
        assertNull(userId);
    }

    /**
     * Tests that {@link UserProcessor#processChangePasswordRequest(String, String, String)} returns true when
     * supplied with a valid request and the user's password is changed successfully
     */
    @Test
    public void processChangePasswordRequest_passwordChangedSuccessfully_returnsTrue() {
        // Arrange
        int userId = 5;
        String username = "jbloggs";
        String currentPassword = "password";
        String newPassword = "newpassword";

        User user = new User();
        user.setId(userId);
        user.setUsername(username);
        user.setEncodedPassword("a7jndsg9");

        when(this.dataStore.getUser(username)).thenReturn(user);
        when(this.passwordEncoder.matches(anyString(), anyString())).thenReturn(true);
        when(this.passwordEncoder.encode(newPassword)).thenReturn(newPassword);

        // Act
        boolean isPasswordUpdated = this.processor.processChangePasswordRequest(username, currentPassword, newPassword);

        // Assert
        assertTrue(isPasswordUpdated);
        verify(this.dataStore, times(1)).getUser(username);
        verify(this.dataStore, times(1)).updateUser(userId, username, newPassword);
    }

    /**
     * Tests that {@link UserProcessor#processChangePasswordRequest(String, String, String)} returns false when there
     * is no user in the database with the supplied username
     */
    @Test
    public void processChangePasswordRequest_noMatchingUsername_returnsFalse() {
        // Arrange
        String username = "jbloggs";
        when(this.dataStore.getUser(username)).thenReturn(null);

        // Act
        boolean isPasswordUpdated = this.processor.processChangePasswordRequest(
                username,
                "current-password",
                "new-password");

        // Assert
        assertFalse(isPasswordUpdated);
        verify(this.dataStore, times(1)).getUser(username);
        verify(this.dataStore, never()).updateUser(anyInt(), anyString(), anyString());
    }

    /**
     * Tests that {@link UserProcessor#processChangePasswordRequest(String, String, String)} returns false when there is
     * a user in the database with the supplied username but the current password provided is incorrect
     */
    @Test
    public void processChangePasswordRequest_incorrectCurrentPasswordSupplied_returnsFalse() {
        // Arrange
        String username = "jbloggs";
        User user = new User();
        user.setUsername(username);
        user.setEncodedPassword("a7jndsg9");

        when(this.dataStore.getUser(username)).thenReturn(user);
        when(this.passwordEncoder.matches(anyString(), anyString())).thenReturn(false);

        // Act
        boolean isPasswordUpdated = this.processor.processChangePasswordRequest(
                username,
                "current-password",
                "new-password");

        // Assert
        assertFalse(isPasswordUpdated);
        verify(this.dataStore, times(1)).getUser(username);
        verify(this.dataStore, never()).updateUser(anyInt(), anyString(), anyString());
    }
}
