package com.personal.projects.climbing.userservice.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * Configures a {@link BCryptPasswordEncoder} to be used throughout the application.
 *
 * @author Chris Smith
 */
@Configuration
public class PasswordEncoderConfig {
    /**
     * Creates a new {@link BCryptPasswordEncoder} instance to be used throughout the application
     *
     * @return the {@link BCryptPasswordEncoder}
     */
    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
