package com.personal.projects.climbing.userservice.data;

/**
 * Contains details of a user that wants to change their password.
 *
 * @author Chris Smith
 */
public class ChangePasswordRequest {
    private String username;
    private String currentPassword;
    private String newPassword;

    /**
     * @return the user's username
     */
    public String getUsername() {
        return this.username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the user's current password
     */
    public String getCurrentPassword() {
        return this.currentPassword;
    }

    /**
     * @param currentPassword the current password to set
     */
    public void setCurrentPassword(String currentPassword) {
        this.currentPassword = currentPassword;
    }

    /**
     * @return the user's desired new password
     */
    public String getNewPassword() {
        return this.newPassword;
    }

    /**
     * @param newPassword the new password to set
     */
    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }
}
