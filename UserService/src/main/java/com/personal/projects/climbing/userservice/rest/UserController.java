package com.personal.projects.climbing.userservice.rest;

import com.personal.projects.climbing.userservice.data.ChangePasswordRequest;
import com.personal.projects.climbing.userservice.data.Constants;
import com.personal.projects.climbing.userservice.data.RegisterUserRequest;
import com.personal.projects.climbing.userservice.processor.UserProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.sql.SQLException;

/**
 * A controller that exposes REST APIs for registering and retrieving user data.
 *
 * @author Chris Smith
 */
@RestController
public class UserController {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserProcessor processor;

    /**
     * REST API for registering a new user
     *
     * @param request a {@link RegisterUserRequest} containing the new user's details
     * @return the unique identifier (integer) of the user that was saved to the database
     */
    @PostMapping(path = "/users", consumes = "application/json")
    public ResponseEntity<?> registerNewUser(@RequestBody RegisterUserRequest request) {
        String username = request.getUsername();
        LOGGER.info("Received request at /users endpoint to register a new user/save a new user to the database. " +
                "Username: {}", username);

        try {
            int userId = this.processor.processNewUserRequest(request);
            LOGGER.info("Successfully registered user with username {} and ID {}", username, userId);
            return new ResponseEntity<Integer>(userId, HttpStatus.OK);
        } catch (DataAccessException e) {
            if (this.isPrimaryKeyError(e)) {
                LOGGER.error("Primary key error - user with the supplied username already exists");
                return new ResponseEntity<String>(
                        String.format("Username %s already taken - new user was not registered", username),
                        HttpStatus.CONFLICT);
            }

            return new ResponseEntity<String>(
                    String.format("Error saving user with username %s to the database", username),
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Authenticates the user with the supplied credentials
     *
     * @param username the username of the user trying to log in
     * @param password the raw password of the user trying to log in
     * @return the unique identifier (integer) of the user if a user with the supplied credentials exists; otherwise a
     * {@link HttpStatus#NOT_FOUND} response
     */
    @GetMapping(path = "/login")
    public ResponseEntity<?> logIn(String username, String password) {
        LOGGER.info("Received request at /login endpoint for user with username {}", username);

        Integer userId;
        try {
            userId = this.processor.processLogInRequest(username, password);
        } catch (DataAccessException e) {
            LOGGER.error("User with username {} could not be retrieved from the database", username);
            return new ResponseEntity<String>(
                    String.format("Error retrieving user with username %s from database", username),
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }

        if (userId != null) {
            LOGGER.info("Successfully retrieved user ID {} for user with username {}", userId, username);
            return new ResponseEntity<Integer>(userId, HttpStatus.OK);
        }

        LOGGER.info("No user exists in the database with the supplied credentials - returning 404 Not Found");
        return new ResponseEntity<String>(
                String.format("Authentication failed for user with username %s", username),
                HttpStatus.NOT_FOUND);
    }

    /**
     * REST API that enables a user to change their password
     *
     * @param request a {@link ChangePasswordRequest} containing details of a user that wants to change their password
     * @return a {@link ResponseEntity} indicating whether the password was changed successfully
     */
    @PostMapping(path = "/change-password", consumes = "application/json")
    public ResponseEntity<?> changePassword(@RequestBody ChangePasswordRequest request) {
        String username = request.getUsername();
        LOGGER.info("Received request at the /change-password endpoint for user with username {}", username);

        boolean isPasswordUpdated;
        try {
            isPasswordUpdated = this.processor.processChangePasswordRequest(
                    username,
                    request.getCurrentPassword(),
                    request.getNewPassword());
        } catch (DataAccessException e) {
            LOGGER.error("{}'s user details could not be updated in the database", username);
            return new ResponseEntity<String>(
                    String.format("Error updating user %s's password in database", username),
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }

        if (isPasswordUpdated) {
            LOGGER.info("Successfully changed password for user {}", username);
            return new ResponseEntity<Void>(HttpStatus.OK);
        }

        LOGGER.info("No user exists in the database with the supplied credentials - returning 404 Not Found");
        return new ResponseEntity<String>(
                String.format("Invalid credentials supplied for user with username %s", username),
                HttpStatus.NOT_FOUND);
    }

    /**
     * Determines if the supplied {@link DataAccessException} was thrown due to a duplicate primary key error
     *
     * @param e the {@link DataAccessException} that was thrown during the attempted database insert
     * @return a boolean indicating whether the exception was thrown due to a duplicate primary key error
     */
    private boolean isPrimaryKeyError(DataAccessException e) {
        if (e.getRootCause() instanceof SQLException) {
            SQLException exception = (SQLException) e.getRootCause();
            return exception.getSQLState().equals(Constants.DUPLICATE_PRIMARY_KEY_SQL_STATE);
        }

        return false;
    }
}
