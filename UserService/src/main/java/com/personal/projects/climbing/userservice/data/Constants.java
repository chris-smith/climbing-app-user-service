package com.personal.projects.climbing.userservice.data;

/**
 * Constants used throughout the User Service.
 *
 * @author Chris Smith
 */
public final class Constants {
    public static final String DUPLICATE_PRIMARY_KEY_SQL_STATE = "23000";

    /**
     * Default private constructor for {@link Constants} to prevent instantiation
     */
    private Constants() {

    }
}
