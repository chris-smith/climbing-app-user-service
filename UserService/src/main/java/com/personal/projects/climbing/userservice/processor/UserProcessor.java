package com.personal.projects.climbing.userservice.processor;

import com.personal.projects.climbing.userservice.data.RegisterUserRequest;
import com.personal.projects.climbing.userservice.data.User;
import com.personal.projects.climbing.userservice.datastore.UserDataStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

/**
 * Processor class for requests related to retrieving and storing user data.
 *
 * @author Chris Smith
 */
@Component
public class UserProcessor {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserProcessor.class);

    @Autowired
    private UserDataStore dataStore;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    /**
     * Processes a request to register a new user
     *
     * @param registerUserRequest the {@link RegisterUserRequest} containing details of the new user
     * @return the unique identifier of the new user
     * @throws DataAccessException on failure to save the user to the database
     */
    public int processNewUserRequest(RegisterUserRequest registerUserRequest) throws DataAccessException {
        String encodedPassword = this.passwordEncoder.encode(registerUserRequest.getPassword());
        return this.dataStore.insertUser(registerUserRequest.getUsername(), encodedPassword);
    }

    /**
     * Processes a request of a user attempting to log in
     *
     * @param username the username of the user trying to log in
     * @param password the raw password of the user trying to log in
     * @return the unique identifier (integer) of the user if a user with the supplied credentials exists; otherwise
     * null
     * @throws DataAccessException on failure to retrieve the user's details from the database
     */
    public Integer processLogInRequest(String username, String password) throws DataAccessException {
        User user = this.dataStore.getUser(username);
        if (user != null && user.getEncodedPassword() != null) {
            if (this.passwordEncoder.matches(password, user.getEncodedPassword())) {
                LOGGER.debug("Supplied password for user {} matches encoded password in database - user " +
                        "authenticated successfully", username);
                return user.getId();
            }

            LOGGER.info("Supplied password for user {} does not match encoded password in database - could not " +
                    "authenticate user", username);
        }

        return null;
    }

    /**
     * Processes a request for a user to change their password
     *
     * @param username        the username of the user wanting to change their password
     * @param currentPassword the user's current password
     * @param newPassword     the user's desired new password
     * @return a boolean indicating whether the user's password was changed successfully
     * @throws DataAccessException on failure to retrieve the user's details from the database
     */
    public boolean processChangePasswordRequest(String username, String currentPassword, String newPassword)
            throws DataAccessException {
        User user = this.dataStore.getUser(username);
        if (user != null && user.getEncodedPassword() != null) {
            if (this.passwordEncoder.matches(currentPassword, user.getEncodedPassword())) {
                this.dataStore.updateUser(user.getId(), username, this.passwordEncoder.encode(newPassword));
                return true;
            }

            LOGGER.info("Supplied current password for user {} does not match encoded password in database - password" +
                    " could not be changed", username);
            return false;
        }

        LOGGER.info("User with username {} does not exist - password could not be changed", username);
        return false;
    }
}
