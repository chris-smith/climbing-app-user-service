package com.personal.projects.climbing.userservice.datastore;

import com.personal.projects.climbing.userservice.data.User;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Maps records from the 'user' database table to {@link User} objects.
 *
 * @author Chris Smith
 */
public class UserRowMapper implements RowMapper<User> {
    @Override
    public User mapRow(ResultSet resultSet, int rowNum) throws SQLException {
        User user = new User();
        user.setId(resultSet.getInt("id"));
        user.setUsername(resultSet.getString("username"));
        user.setEncodedPassword(resultSet.getString("password"));
        return user;
    }
}
