package com.personal.projects.climbing.userservice.data;

/**
 * Stores information about a user - maps to a record from the 'user' database table.
 *
 * @author Chris Smith
 */
public class User {
    private int id;
    private String username;
    private String encodedPassword;

    /**
     * @return the unique identifier for the user
     */
    public int getId() {
        return this.id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the user's username
     */
    public String getUsername() {
        return this.username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the user's encoded password
     */
    public String getEncodedPassword() {
        return this.encodedPassword;
    }

    /**
     * @param encodedPassword the encoded password to set
     */
    public void setEncodedPassword(String encodedPassword) {
        this.encodedPassword = encodedPassword;
    }
}
