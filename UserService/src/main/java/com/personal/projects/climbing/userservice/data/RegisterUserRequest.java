package com.personal.projects.climbing.userservice.data;

/**
 * Contains details of a user that wants to register for an account on the Climbing App.
 *
 * @author Chris Smith
 */
public class RegisterUserRequest {
    private String username;
    private String password;

    /**
     * @return the user's username
     */
    public String getUsername() {
        return this.username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the user's raw password
     */
    public String getPassword() {
        return this.password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }
}
