package com.personal.projects.climbing.userservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.autoconfigure.security.servlet.ManagementWebSecurityAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

/**
 * Entry point for the User Service application.
 *
 * @author Chris Smith
 */
@SpringBootApplication(exclude = {SecurityAutoConfiguration.class, ManagementWebSecurityAutoConfiguration.class})
public class UserService {
    /**
     * Entry point method for the {@link UserService}
     *
     * @param args string array of command line arguments
     */
    public static void main(String[] args) {
        SpringApplication.run(UserService.class, args);
    }
}
