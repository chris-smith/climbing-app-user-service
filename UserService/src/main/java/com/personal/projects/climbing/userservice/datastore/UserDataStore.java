package com.personal.projects.climbing.userservice.datastore;

import com.personal.projects.climbing.userservice.data.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.Types;
import java.util.List;
import java.util.Map;

/**
 * Data access class for the 'user' database table.
 *
 * @author Chris Smith
 */
@Repository
public class UserDataStore {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserDataStore.class);

    private SimpleJdbcCall procInsertUser;
    private SimpleJdbcCall procGetUser;
    private SimpleJdbcCall procUpdateUser;

    /**
     * Sets the data source and related metadata so the stored procedures can be accessed in the data source
     *
     * @param dataSource a connection to the data source
     */
    @Autowired
    public void setDataSource(DataSource dataSource) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.setResultsMapCaseInsensitive(true);
        this.setStoredProcedures(jdbcTemplate);
    }

    /**
     * Inserts a user with the supplied details into the database
     *
     * @param username the user's username
     * @param password the user's encoded password
     * @return the unique identifier of the user in the database
     * @throws DataAccessException on failure to save the user to the database
     */
    public int insertUser(String username, String password) throws DataAccessException {
        LOGGER.info("Inserting user with username {} into database", username);

        SqlParameterSource inputParams = new MapSqlParameterSource()
                .addValue("username_in", username)
                .addValue("password_in", password);

        try {
            Map<String, Object> result = this.procInsertUser.execute(inputParams);
            return (Integer) result.get("user_id_out");
        } catch (DataAccessException dae) {
            LOGGER.error("Failed to insert user with username {} into database", username, dae);
            throw dae;
        }
    }

    /**
     * Retrieves the {@link User} with the supplied username from the database
     *
     * @param username the user's username
     * @return the {@link User} if exists; otherwise null
     * @throws DataAccessException on failure to connect to the database
     */
    public User getUser(String username) throws DataAccessException {
        LOGGER.info("Retrieving user with username {} from database", username);
        SqlParameterSource inputParams = new MapSqlParameterSource().addValue("username_in", username);

        try {
            Map<String, Object> result = this.procGetUser.execute(inputParams);
            return result.get("users") != null && ((List<User>) result.get("users")).size() > 0
                    ? ((List<User>) result.get("users")).get(0) : null;
        } catch (DataAccessException dae) {
            LOGGER.error("Failed to retrieve user with username {} from database", username, dae);
            throw dae;
        }
    }

    /**
     * Updates the user record with the specified user ID with the supplied details
     *
     * @param userId   the unique identifier of the user record to update
     * @param username the username to set
     * @param password the encoded password to set
     * @throws DataAccessException on failure to update the user's details
     */
    public void updateUser(int userId, String username, String password) throws DataAccessException {
        LOGGER.info("Updating user details for user {}", userId);

        SqlParameterSource inputParams = new MapSqlParameterSource()
                .addValue("user_id_in", userId)
                .addValue("username_in", username)
                .addValue("password_in", password);

        try {
            this.procUpdateUser.execute(inputParams);
        } catch (DataAccessException dae) {
            LOGGER.error("Failed to update user details for user {}", userId, dae);
            throw dae;
        }
    }

    /**
     * Sets the metadata for the stored procedures in this data source
     *
     * @param jdbcTemplate the JDBC template that gives access to the data source
     */
    private void setStoredProcedures(JdbcTemplate jdbcTemplate) {
        this.procInsertUser = new SimpleJdbcCall(jdbcTemplate)
                .withProcedureName("usp_user_insert")
                .declareParameters(new SqlOutParameter("user_id_out", Types.INTEGER));

        this.procGetUser = new SimpleJdbcCall(jdbcTemplate)
                .withProcedureName("usp_user_get")
                .returningResultSet("users", new UserRowMapper());

        this.procUpdateUser = new SimpleJdbcCall(jdbcTemplate)
                .withProcedureName("usp_user_update");
    }
}
