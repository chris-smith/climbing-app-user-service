#!/bin/bash

if "$WAIT_FOR_MYSQL"
then
	until nc -z -v -w1 databasehost 3306 > /dev/null
	do
		echo "MariaDB not available"
		sleep 1
	done
	echo "MariaDB accepting connections. Deploying database."
else
	echo "Not waiting for MariaDB. Attempting to deploy database."
fi

# Remove empty lines and comments
echo "Removing comments/empty lines from deployment script"
sed -i '/^#/d' deployment_files.xml
sed -i '/^[[:space:]]*$/d' deployment_files.xml

# Run each SQL script
while read line; do
	line=${line#*<script>}
	script=${line%%</script>*}
	
	# Replace user (db) user password placeholder with password from env var
	if [[ $script == *"userdbuser"* ]]
	then
		echo "Injecting password into user (db) user script"
		sed -i "s/USER_DB_USER_PASSWORD_PLACEHOLDER/${USER_DB_USER_PASSWORD}/g" $script
	fi

	echo "Running $script"
	mysql -u root -p"${MYSQL_ROOT_PASSWORD}" -h databasehost -P 3306 2>&1 < $script | grep -v "Using a password"
done < ./deployment_files.xml

echo "Database deployment complete."
