CREATE USER IF NOT EXISTS 'userdb_user'@'%' IDENTIFIED BY 'USER_DB_USER_PASSWORD_PLACEHOLDER';

GRANT EXECUTE ON `user`.* TO 'userdb_user'@'%';
GRANT SELECT ON mysql.proc TO 'userdb_user'@'%';

COMMIT;
