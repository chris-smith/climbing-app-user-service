DELIMITER //

USE `user`//

CREATE TABLE IF NOT EXISTS `user`.`user`
(
	`id` INT NOT NULL AUTO_INCREMENT COMMENT 'Unique identifier for the user',
	`username` VARCHAR(100) NOT NULL COMMENT 'The user\'s username',
	`password` VARCHAR(64) NOT NULL COMMENT 'The user\'s encrypted password',
	PRIMARY KEY (id),
	UNIQUE KEY (username)
) ENGINE=InnoDB COMMENT='Stores user details' CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci//

COMMIT//

DELIMITER ;
