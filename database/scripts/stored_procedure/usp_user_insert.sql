DELIMITER //

DROP PROCEDURE IF EXISTS `user`.`usp_user_insert`//

CREATE PROCEDURE `user`.`usp_user_insert`(
	IN 	username_in VARCHAR(100),
	IN 	password_in	VARCHAR(64),
	OUT user_id_out INT
)
BEGIN

	INSERT INTO `user`.`user`(`username`, `password`)
	VALUES (username_in, password_in);
	
	SELECT LAST_INSERT_ID() INTO user_id_out;

END//

COMMIT//

DELIMITER ;
