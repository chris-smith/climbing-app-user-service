DELIMITER //

DROP PROCEDURE IF EXISTS `user`.`usp_user_update`//

CREATE PROCEDURE `user`.`usp_user_update`(
	IN user_id_in 	INT,
	IN username_in	VARCHAR(100),
	IN password_in	VARCHAR(64)
)
BEGIN

	UPDATE 	`user`.`user`
	SET		username = username_in,
			password = password_in
	WHERE 	id = user_id_in;

END//

COMMIT//

DELIMITER ;
