DELIMITER //

DROP PROCEDURE IF EXISTS `user`.`usp_user_delete`//

CREATE PROCEDURE `user`.`usp_user_delete`(
	IN user_id_in 		INT
)
BEGIN

	DELETE FROM	`user`.`user`
	WHERE id = user_id_in;

END//

COMMIT//

DELIMITER ;
