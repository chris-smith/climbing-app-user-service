DELIMITER //

DROP PROCEDURE IF EXISTS `user`.`usp_user_get`//

CREATE PROCEDURE `user`.`usp_user_get`(
	IN username_in VARCHAR(100)
)
BEGIN

	SELECT	id, username, password
	FROM	`user`.`user`
	WHERE	username = username_in;

END//

COMMIT//

DELIMITER ;
